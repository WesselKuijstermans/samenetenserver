const express = require("express")
const app = express()
const port = process.env.PORT || 3000
var logger = require('tracer').console()
app.use(express.json())

const studentHome = require("./router/routerStudentHome")
const authentication = require("./controller/controllerAuthentication")

app.use("/api", studentHome)
app.post("/api/register/", 
  authentication.validateRegister,
  authentication.register
)

app.post("/api/login", authentication.validateLogin,
authentication.login
)

app.listen(port, () => {
  logger.log(`example app listening at http://localhost:${port}`)
})

module.exports = app