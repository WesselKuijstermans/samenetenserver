const timeToWait = 1500;
const logger = require("tracer").console();
const database = require("../dao/database");
const assert = require("assert");
const { dbconfig } = require("../dao/databaseConfig");

module.exports = {
  validateNewMeal(req, res, next) {
    logger.log("Validate aangeroepen");
    try {
      const naam = req.query.naam;
      logger.log(naam);
      const beschrijving = req.query.beschrijving;
      const ingredienten = req.query.ingredienten;
      const allergien = req.query.allergien;
      const aangebodenOp = req.query.aangebodenOp;
      const prijs = req.query.prijs;
      const studenthomeId = req.params["id"];
      const aantalPersonen = req.query.aantalPersonen;

      assert(typeof naam === "string", "naam is missing");
      assert(typeof beschrijving === "string", "beschrijving is missing");
      assert(typeof ingredienten === "string", "ingredienten is missing");
      assert(typeof allergien === "string", "allergien is missing");
      assert(typeof aangebodenOp === "string", "aangebodenOp is missing");
      assert(typeof prijs === "string", "prijs is missing");
      assert(typeof studenthomeId === "string", "StudenthomeId is missing");
      assert(typeof aantalPersonen === "string", "aantalpersonen is missing");

      assert.match(
        aangebodenOp,
        /[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/
      );
      logger.log("validate new meal is accepted");

      next();
    } catch (err) {
      logger.log("Invalid data", err.message);
      res.status(400).send({
        Message:
          "Een of meer properties in de request query ontbreken of zijn foutief",
      });
    }
  },

  postNewMeal(req, res, next) {
    logger.log("post new meal aangeroepen");
    var date = new Date();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    const naam = req.query.naam;
    const beschrijving = req.query.beschrijving;
    const ingredienten = req.query.ingredienten;
    const allergien = req.query.allergien;
    const gemaaktOp = year + "-" + month + "-" + day;
    const aangebodenOp = req.query.aangebodenOp;
    const prijs = req.query.prijs;
    const userid = req.userId;
    const studenthomeId = req.params["id"];
    const aantalPersonen = req.query.aantalPersonen;

    const query = {
      sql: "INSERT INTO meal (Name,Description, Ingredients,Allergies,CreatedOn,OfferedOn,Price,UserID,StudenthomeID,MaxParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
      values: [
        naam,
        beschrijving,
        ingredienten,
        allergien,
        gemaaktOp,
        aangebodenOp,
        prijs,
        userid,
        studenthomeId,
        aantalPersonen,
      ],
      timeout: timeToWait,
    };

    database.query(query, (error, rows, fields) => {
      if (error) {
        res.status(500).json(error.toString());
      } else {
        res.status(200).send({
          Message: "Meal is created",
        });
      }
    });
  },

  updateMeal(req, res, next) {
    logger.log("Update meal aangeroepen");
    const id = req.params["id"];
    const mealID = req.params["mealID"];
    var date = new Date();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    const naam = req.query.naam;
    const beschrijving = req.query.beschrijving;
    const ingredienten = req.query.ingredienten;
    const allergien = req.query.allergien;
    const gemaaktOp = year + "-" + month + "-" + day;
    const aangebodenOp = req.query.aangebodenOp;
    const prijs = req.query.prijs;
    const userid = req.userId;
    const studenthomeId = req.params["id"];
    const aantalPersonen = req.query.aantalPersonen;

    const queryCheck = {
      sql: "SELECT * FROM meal WHERE ID = ?",
      values: [mealID],
      timeout: timeToWait,
    };

    database.query(queryCheck, (error, rows, fields) => {
      if (rows != 0) {
        if (id && mealID) {
          logger.log(id, mealID);
          const query = {
            sql: "SELECT * FROM meal WHERE ID = ? AND StudenthomeID = ? AND UserID = ?",
            values: [mealID, id, userid],
            timeout: timeToWait,
          };
          database.query(query, (error, rows, fields) => {
            if (error) {
              res.status(500).json(error.toString());
            } else if (rows != 0) {
              logger.log("Has a meal existing");
              const query = {
                sql: "UPDATE meal SET Name = ?, Description =?, Ingredients = ? ,Allergies =?, CreatedOn = ?, OfferedOn = ?, Price = ?,UserID = ?,StudenthomeID = ?, MaxParticipants = ?",
                values: [
                  naam,
                  beschrijving,
                  ingredienten,
                  allergien,
                  gemaaktOp,
                  aangebodenOp,
                  prijs,
                  userid,
                  studenthomeId,
                  aantalPersonen,
                ],
                timeout: timeToWait,
              };
              database.query(query, (error, rows, fields) => {
                if (error) {
                  res.status(500).json(error.toString());
                } else {
                  res.status(200).json("Meal: " + mealID + " Updated succesfuly");
                }
              });
            } else {
              res
                .status(401)
                .send({
                  message: "You do not have permission to update this meal",
                });
            }
          });
        } else {
          res.status(400).send({
            Message: "Invalid houseId and invalid mealId or missing id",
          });
        }
      } else {
        res.status(404).send({
          Message: "Meal does not exist"
        })
      }
    });
  },

  getMeals(req, res, next) {
    logger.log("getMeals called")
    const studenthomeID = req.params["id"];

    const query = {
      sql: "SELECT ID, Name, Description, OfferedOn, Price, MaxParticipants FROM meal WHERE StudenthomeID = ?",
      values: [studenthomeID],
      timeout: timeToWait,
    };
    database.query(query, (error, rows, fields) => {
      if (error) {
        res.status(500).json(error.toString());
      } else {
        res.status(200).json(rows);
      }
    });
  },

  getMealInformation(req, res, next) {
    logger.log("getMealInformation called")
    const studenthomeID = req.params["id"];
    const mealID = req.params["mealID"];

    const query = {
      sql: "SELECT * FROM meal WHERE ID = ? AND StudenthomeID = ?",
      values: [mealID, studenthomeID],
      timeout: timeToWait,
    };
    database.query(query, (error, rows, fields) => {
      if (error) {
        res.status(500).json(error.toString());
      } else if (rows == 0) {
        res.status(404).send({
          Message: "Meal was not found"
        })
      } else {
        res.status(200).json(rows);
      }
    });
  },

  deleteMeal(req, res, next) {
    logger.log("deleteMeal called")
    const mealId = req.params["mealID"];
    const userId = req.userId;

    const queryCheck = {
      sql: "SELECT * FROM meal WHERE ID = ?",
      values: [mealId],
      timeout: timeToWait,
    };

    const querySelect = {
      sql: "SELECT * FROM meal WHERE ID = ? && UserID = ?",
      values: [mealId, userId],
      timeout: timeToWait,
    };

    const query = {
      sql: "DELETE FROM meal WHERE ID = ? && UserID = ?",
      values: [mealId, userId],
      timeout: timeToWait,
    };
    database.query(queryCheck, (error, rows, fields) => {
      if (rows != 0) {
        database.query(querySelect, (error, rows, fields) => {
          if (error) {
            res.status(500).json(error.toString());
          } else if (rows != 0) {
            database.query(query, (error, rows, fields) => {
              logger.log(rows);

              if (error) {
                res.status(500).json(error.toString());
              } else {
                res.status(200).send({
                  Message: "Verwijderd "
                });
              }
            });
          } else {
            res
              .status(401)
              .json("You dont have the autorisation to delete this meal");
          }
        });
      } else {
        res.status(404).send({
          Message: "No Meal was found with the given ID"
        })
      }
    })

  },
};
