const timeToWait = 1500;
const logger = require("tracer").console();
const database = require("../dao/database");
const assert = require("assert");
const jwt = require("jsonwebtoken");
const jwtSecretKey = require("../dao/databaseConfig").jwtSecretKey;

module.exports = {
  login(req, res, next) {
    database.query(
      "SELECT `ID`, `Email`, `Password`, `First_Name`, `Last_Name` FROM `user` WHERE `Email` = ?",
      [req.query.email],

      (err, rows, fields) => {
        if (err) {
          logger.error("Error: ", err.toString());
          res.status(500).json({
            error: err.toString(),
            datetime: new Date().toISOString(),
          });
        } else {
          // 2. Er was een resultaat, check het password.
          logger.info("Result from database: ");
          logger.info(rows);
          if (
            rows &&
            rows.length === 1 &&
            rows[0].Password == req.query.password
          ) {
            logger.info("passwords DID match, sending valid token");
            // Create an object containing the data we want in the payload.
            const payload = {
              id: rows[0].ID,
            };
            // Userinfo returned to the caller.
            const userinfo = {
              id: rows[0].ID,
              firstName: rows[0].First_Name,
              lastName: rows[0].Last_Name,
              email: rows[0].Email,
              token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
            };
            logger.debug("Logged in, sending: ", userinfo);
            res.status(200).json(userinfo);
          } else {
            logger.info("User not found or password invalid");
            res.status(401).json({
              message: "User not found or password invalid",
              datetime: new Date().toISOString(),
            });
          }
        }
      }
    );
  },

  //
  //
  //
  validateLogin(req, res, next) {
    // Verify that we receive the expected input
    try {
      assert(typeof req.query.email === "string", "email must be a string.");
      assert(
        typeof req.query.password === "string",
        "password must be a string."
      );
      next();
    } catch (ex) {
      res.status(422).json({ error: ex.toString(), datetime: new Date().toISOString() });
    }
  },

  //
  //
  //
  register(req, res, next) {
    logger.info("register");
    logger.info(req.query);
    let { firstname, lastname, email, studentnr, password } = req.query;
    database.query(
      "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)",
      [firstname, lastname, email, studentnr, password],
      (err, rows, fields) => {
        if (err) {
          // When the INSERT fails, we assume the user already exists
          logger.error("Error: " + err.toString());
          res.status(400).json({
            message: "This email has already been taken.",
            datetime: new Date().toISOString(),
          });
        } else {
          logger.trace(rows);
          // Create an object containing the data we want in the payload.
          // This time we add the id of the newly inserted user
          const payload = {
            id: rows.insertId,
          };
          // Userinfo returned to the caller.
          const userinfo = {
            id: rows.insertId,
            firstName: firstname,
            lastName: lastname,
            email: email,
            studentnr: studentnr,
            token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2H" }),
          };
          logger.debug("Registered", userinfo);
          res.status(200).json(userinfo);
        }
      }
    );
  },

  //
  //
  //
  validateRegister(req, res, next) {
    logger.log("is aangeroepen");
    // Verify that we receive the expected input
    var test = /^[^\s@]+@[^\s@]+$/;
    try {
      assert(typeof req.query.firstname === "string", "firstname must be a string.");

      assert(typeof req.query.lastname === "string", "lastname must be a string.");

      assert(typeof req.query.email === "string", "email must be a string.");
      
      assert.match(req.query.email, test);

      assert(typeof req.query.studentnr === "string", "studentnr must be a integer.");

      assert(typeof req.query.password === "string", "password must be a string.");
      next();
    } catch (ex) {
      logger.debug("validateRegister error: ", ex.toString());
      res
        .status(400)
        .json({ message: ex.toString(), datetime: new Date().toISOString() });
    }

  },

  validateToken(req, res, next) {
    logger.info("validateToken called");
    // logger.trace(req.headers)
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      res.status(401).send({
        Message: "Not logged in"
      })
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn("Not authorized");
          res.status(401).send({
            Message: "Not authorized"
          })
        }
        if (payload) {
          logger.debug("token is valid", payload);
          // User heeft toegang. Voeg UserId uit payload toe aan
          // request, voor ieder volgend endpoint.
          req.userId = payload.id;
          next();
        }
      });
    }
  },

  renewToken(req, res, next) {
    logger.debug("renewToken");
    database.query(
      "SELECT * FROM `user` WHERE `ID` = ?",
      [req.userId],
      (err, rows, fields) => {
        if (err) {
          logger.error("Error: ", err.toString());
          res.status(500).json({
            error: err.toString(),
            datetime: new Date().toISOString(),
          });
        } else {
          // 2. User gevonden, return user info met nieuw token.
          // Create an object containing the data we want in the payload.
          const payload = {
            id: rows[0].ID,
          };
          // Userinfo returned to the caller.
          const userinfo = {
            id: rows[0].ID,
            firstName: rows[0].First_Name,
            lastName: rows[0].Last_Name,
            emailAdress: rows[0].Email,
            token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
          };
          logger.debug("Sending: ", userinfo);
          res.status(200).json(userinfo);
        }
      }
    );
  },
};
