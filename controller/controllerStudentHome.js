const timeToWait = 1500;

const logger = require("tracer").console();
const database = require("../dao/database");
const assert = require("assert");
const { dbconfig } = require("../dao/databaseConfig");

module.exports = {
  getStudentHomeById(req, res, next) {
    logger.log("ID aangeroepen");
    const id = req.params.id;
    const query = {
      sql: "SELECT * FROM studenthome WHERE ID = ?",
      values: [id],
      timeout: timeToWait,
    };

    database.query(query, (error, rows, fields) => {
      if (error) {
        res.status(400).json(error.toString());
      } else if (rows.length == 1) {
        res.status(200).json({
          Message: rows,
        });
        logger.log(rows.length);
        next();
      } else {
        res.status(404).json({
          Message: "The ID is not valid or does not exist",
        });
      }
    });
  },
  validateUniquePostalCodeAdress(req, res, next) {
    logger.log("Deze werkt wel");
    const postcode = req.query.postcode;
    const huisnummer = req.query.huisnummer;
    const query = {
      sql: "SELECT * FROM studenthome WHERE House_Nr = ? AND Postal_Code = ?",
      values: [huisnummer, postcode],
      timeout: timeToWait,
    };

    database.query(query, (error, rows, fields) => {
      logger.log(rows);
      if (error) {
        res.status(400).json(error.toString());
      } else if (rows.length == 0) {
        logger.log(rows.length);
        next();
      } else {
        res.status(404).json({
          Message: "A studenthouse with this adress already exist",
        });
      }
    });
  },

  validatePostalCodeAndCity(req, res, next) {
    logger.log("validatePostalCodeAndCity called")
    try {
      const naam = req.query.naam;
      const straatnaam = req.query.straatnaam;
      const huisnummer = req.query.huisnummer;
      const postcode = req.query.postcode;
      const plaats = req.query.plaats;
      const telefoonnummer = req.query.telefoonnummer;

      assert(typeof naam === "string", "naam is missing");
      assert(typeof straatnaam === "string", "straatnaam is missing");
      assert(typeof huisnummer === "string", "huisnummer is missing");
      assert(typeof postcode === "string", "postcode is missing");
      assert(typeof plaats === "string", "plaats is missing");
      assert(typeof telefoonnummer === "string", "telefoonnummer is missing");

      assert.match(postcode, /[1-9]{1}[0-9]{3}[a-zA-Z]{2}/);
      assert.match(telefoonnummer, /[0]{1}[6]{1}[0-9]{8}/);

      logger.log("postal code and city are validated");
      next();
    } catch (err) {
      logger.log("data is invalid:", err.message);
      res.status(400).json({
        Message:
          "Een of meer properties in de request query ontbreken of zijn foutief",
      });
    }
  },
  //TODO geef door aan de mannen lijn 32,
  //TODO user id koppelen ipv standaard userID maken doe dit door tokens
  postNewStudentHome(req, res, next) {
    logger.log("postNewStudentHome called")
    const naam = req.query.naam;
    const straatnaam = req.query.straatnaam;
    const huisnummer = req.query.huisnummer;
    const postcode = req.query.postcode;
    const plaats = req.query.plaats;
    const telefoonnummer = req.query.telefoonnummer;
    const userid = req.userId;

    const query = {
      sql: "INSERT INTO studenthome(Name,Address,House_Nr,UserID,Postal_Code, Telephone, City) VALUES (?, ?, ?, ?, ?, ?, ?)",
      values: [
        naam,
        straatnaam,
        huisnummer,
        userid,
        postcode,
        telefoonnummer,
        plaats,
      ],
      timeout: timeToWait,
    };

    database.query(query, (error, rows, fields) => {
      if (error) {
        res.status(500).json(error.toString());
      } else {
        res.status(200).send({
          Message: "Nieuwe studentenhuis toegevoegd",
        });
      }
    });
  },

  getStudentHomes(req, res, next) {
    logger.log("Get studentHomes aangeroepen");
    if (Object.keys(req.query).length === 0) {
      const query = {
        sql: "SELECT * FROM studenthome",
        timeout: timeToWait,
      };

      database.query(query, (error, rows, fields) => {
        if (error) {
          res.status(500).json(error.toString());
        } else {
          res.status(200).json(rows);
        }
      });
    } else {
      next();
    }
  },

  getStudentHomeByName(req, res, next) {
    logger.log("Get studentHomeByName aangeroepen");
    const naam = "%" + req.query.naam + "%";

    if (req.query.naam) {
      const query = {
        sql: "SELECT * FROM studenthome WHERE Name LIKE ?",
        values: [naam],
        timeout: timeToWait,
      };
      database.query(query, (error, rows, fields) => {
        if (rows.length === 0) {
          res.status(404).send({
            Message: "Naam studentenhuis niet gevonden",
          });
          next();
        } else if (error) {
          res.status(400).send({
            Message: "Unidentified error: " + error.code,
          });
          next();
        } else {
          res.status(200).json(rows);
        }
      });
    } else if (req.query.plaats) {
      next();
    } else {
      res.status(404).send({
        Message: "Gebruik parameter naam of plaats",
      });
    }
  },

  getStudentHomeByPlace(req, res, next) {
    logger.log("Get studentHomeByCity aangeroepen");
    const plaats = "%" + req.query.plaats + "%";

    if (req.query.plaats) {
      const query = {
        sql: "SELECT * FROM studenthome WHERE City LIKE ?",
        values: [plaats],
        timeout: timeToWait,
      };
      database.query(query, (error, rows, fields) => {
        if (rows.length === 0) {
          res.status(404).send({
            Message: "Plaats studentenhuis niet gevonden",
          });
        } else if (error) {
          res.status(400).send({
            Message: "Unidentified error: " + error.code,
          });
        } else {
          res.status(200).json(rows);
        }
      });
    } else {
      res.status(404).send({
        Message: "Gebruik parameter naam of plaats",
      });
    }
  },

  updateStudentHome(req, res) {
    logger.log("updateStudentHome aangeroepen");
    const id = req.params["id"];
    const userid = req.userId;
    const naam = req.query.naam;
    const straatnaam = req.query.straatnaam;
    const huisnummer = req.query.huisnummer;
    const plaats = req.query.plaats;
    const postcode = req.query.postcode;
    const telefoonnummer = req.query.telefoonnummer;

    if (id) {
      const query = {
        sql: "SELECT * FROM studenthome WHERE ID = ?",
        values: [id],
        timeout: timeToWait,
      };
      database.query(query, (error, rows, fields) => {
        if (rows.length == 0) {
          res.status(404).send({
            Message: "Studenthome does not exist",
          });
        } else if (userid) {
          const query = {
            sql: "SELECT * FROM studenthome WHERE ID = ? AND UserID = ?",
            values: [id, userid],
            timeout: timeToWait,
          };
          database.query(query, (error, rows, fields) => {
            if (error) {
              res.status(401).send({
                Message: "You do not have permission to update this studenthome",
              });
            } else if (rows.length != 0) {
              const query = {
                sql: "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?,Postal_Code = ?,Telephone = ?,City = ? WHERE ID= ? AND UserID = ?",
                values: [
                  naam,
                  straatnaam,
                  huisnummer,
                  postcode,
                  telefoonnummer,
                  plaats,
                  id,
                  userid,
                ],
                timeout: timeToWait,
              };
              database.query(query, (error, rows, fields) => {
                if (rows.length != 0) {
                  res.status(200).send({
                    Message: "ID: " + id + " updated",
                  });
                }
              });
            }
          });
        }
      });
    }
  },

  deleteStudentHome(req, res) {
    logger.log("deleteStudentHome called")
    const id = req.params["id"];
    const userid = req.userId;
    if (id) {
      const query = {
        sql: "DELETE FROM studenthome WHERE ID = ? AND UserID = ?",
        values: [id, userid],
        timeout: timeToWait,
      };

      database.query(query, (error, rows, fields) => {
        if (error) {
          res.status(500).send({
            Message: "Either no studenthome was found with the given ID or the studenthome was found, but you do not have the required authorisation",
          });
        } else if (rows.affectedRows == 0) {
          const query = {
            sql: "SELECT * FROM studenthome WHERE ID = ?",
            values: [id],
            timeout: timeToWait,
          };
          database.query(query, (error, rows, fields) => {
            if (rows == 0) {
              res.status(404).send({
                Message: "StudentHome was not found",
              });
            } else {
              res.status(401).send({
                Message: "You do not have permission to delete this studenhome",
              });
            }
          });
        } else {
          res.status(200).send({
            Message: "ID: " + id + " deleted",
          });
        }
      });
    }
  },
};
