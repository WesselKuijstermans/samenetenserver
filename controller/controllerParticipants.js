const express = require('express');
const logger = require('tracer').console();
const database = require("../dao/database");
const timeToWait = 1500;

module.exports = {
    // UC-401 Aanmelden voor maaltijd
    signupForMeal(req, res) {
        logger.log('signupForMeal aangeroepen');
        let userID = req.userId;
        let homeID = req.params.homeId;
        let mealID = req.params.mealId;
        let signedUpOn = new Date().toISOString();
        let query = {
            sql: "SELECT * FROM `meal` WHERE MealID = ?",
            values: [mealID],
            timeout: timeToWait
        };

        database.query(query, (error, rows, fields) => {
            if (rows == 0) {
                res.status(404).send({
                    Message: "The given meal does not exist"
                })
            } else {
                if (userID != null) {
                    query = {
                        sql: "SELECT * FROM `participants` WHERE MealID = ? AND StudenthomeID = ? AND UserID = ?",
                        values: [mealID, homeID, userID],
                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (error) {
                            res.status(404).send({
                                Message: 'Enter a valid MealID'
                            })
                        } else if (rows == 0) {
                            query = {
                                sql: "INSERT INTO participants(UserID,StudenthomeID,MealID,SignedUpOn) VALUES (?, ?, ?, ?)",
                                values: [userID, homeID, mealID, signedUpOn],
                                timeout: timeToWait
                            }
                            database.query(query, (error, rows, fields) => {
                                if (error) {
                                    res.status(404).send({
                                        Message: 'Meal niet gevonden vul een geldige ID in'
                                    })
                                } else {
                                    res.status(200).send({
                                        Message: "You signed up to the meal!"
                                    });
                                }
                            })
                        } else {
                            res.status(404).send({
                                Message: "You already have signed up to the meal!"
                            });
                        }
                    });
                } else {
                    res.status(401).send({
                        Message: "You need to be logged in to sign up"
                    })
                }
            }
        })

    },

    // UC-402 Afmelden voor maaltijd
    signoffForMeal(req, res) {
        logger.log('signoffForMeal aangeroepen');
        let userID = req.userId;
        let homeID = req.params.homeId;
        let mealID = req.params.mealId;

        if (userID) {
            query = {
                sql: "SELECT * FROM `participants` WHERE MealID = ? AND StudenthomeID = ? AND UserID = ?",
                values: [mealID, homeID, userID],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(404).send({
                        Message: 'Registration niet gevonden vul een geldige ID in'
                    })
                } else if (rows != 0) {
                    query = {
                        sql: "DELETE FROM `participants` WHERE `StudenthomeID`= ? AND `MealID` = ? AND `UserID` = ?",
                        values: [homeID, mealID, userID],
                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (error) {
                            res.status(404).send({
                                Message: 'Meal niet gevonden vul een geldige ID in'
                            })
                        } else {
                            res.status(200).send({
                                Message: "You signed off to the meal!"
                            });
                        }
                    })
                } else {
                    res.status(404).send({
                        Message: 'Meal of registration niet gevonden, vul een geldige ID in.'
                    });
                }
            });
        }
    },

    // UC-403 Lijst van deelnemers opvragen
    getParticipants(req, res) {
        logger.log('getMealById aangeroepen');
        let mealID = req.params.mealId;

        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "SELECT UserID, StudenthomeID, Student_Number, First_Name, Last_Name, SignedUpOn FROM `participants` RIGHT JOIN user ON participants.UserID = user.ID WHERE MealID = ?",
                values: [mealID],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString());
                } else if (rows == 0) {
                    res.status(404).json({
                        Message: 'No participants found'
                    });
                } else {
                    res.status(200).json(rows)
                }
            })
        }
    },

    // UC-404 Details van deelnemer opvragen
    getDetailsParticipants(req, res) {
        logger.log('getDetailsParticipants aangeroepen');
        let mealID = req.params.mealId;
        let participantID = req.params.participantId;

        const query = {
            sql: "SELECT * FROM participants WHERE MealID = ? AND UserID = ?",
            values: [mealID, participantID],
            timeout: timeToWait
        }
        database.query(query, (error, rows, field) => {
            if (rows.length != 0) {
                if (Object.keys(req.query).length === 0) {
                    const query = {
                        sql: "SELECT UserID, StudenthomeID, Student_Number, First_Name, Last_Name, SignedUpOn FROM `user` RIGHT JOIN participants ON participants.UserID = user.ID WHERE MealID = ? AND UserID = ?",
                        values: [mealID, participantID],
                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (error) {
                            res.status(500).json(error.toString());
                        } else {
                            res.status(200).json(rows);
                        }
                    })
                }
            } else {
                res.status(404).send({
                    Message: "The given UserID either doesn't exist or isn't signed up to the meal"
                })
            }
        })

    }
}