const { logger } = require("../dao/databaseConfig")

module.exports = {
    sendInfo(req,res) {
        logger.log("sendInfo called")
        res.status(200).send({
            Naam: "Wessel Kuijstermans",
            Studentennummer: "2166881",
            Informatie: "In deze API kan je je jezelf registreren om vervolgens een studentenhuis aan te maken een hier beschikbare maaltijden aan toevoegen",
        })

    }  
}