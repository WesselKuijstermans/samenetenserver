var express = require("express");
var logger = require("tracer").console();
const controllerStudentHome = require("../controller/controllerStudentHome");
const controllerMeals = require("../controller/controllerMeals");
const controllerInfo = require("../controller/controllerInfo")
const auth = require("../controller/controllerAuthentication");
const { validateLogin } = require("../controller/controllerAuthentication");
const controllerSignUp = require("../controller/controllerParticipants");
var router = express.Router();

router.get("/info", controllerInfo.sendInfo)

router.post(
  "/studenthome",
  auth.validateToken,
  controllerStudentHome.validatePostalCodeAndCity,
  controllerStudentHome.validateUniquePostalCodeAdress,
  controllerStudentHome.postNewStudentHome
);

router.get("/studenthome", controllerStudentHome.getStudentHomes);

router.get("/studenthome", controllerStudentHome.getStudentHomeByName);

router.get("/studenthome", controllerStudentHome.getStudentHomeByPlace);

router.get("/studenthome/:id", controllerStudentHome.getStudentHomeById);

router.put(
  "/studenthome/:id",
  auth.validateToken,
  controllerStudentHome.validatePostalCodeAndCity,
  controllerStudentHome.validateUniquePostalCodeAdress,
  controllerStudentHome.updateStudentHome
);

router.delete(
  "/studenthome/:id",
  auth.validateToken,
  controllerStudentHome.deleteStudentHome
);

router.post(
  "/studenthome/:id/meal",
  auth.validateToken,
  controllerMeals.validateNewMeal,
  controllerMeals.postNewMeal
);

router.put(
  "/studenthome/:id/meal/:mealID",
  auth.validateToken,
  controllerMeals.validateNewMeal,
  controllerMeals.updateMeal
);

router.get("/studenthome/:id/meal", controllerMeals.getMeals);

router.get("/studenthome/:id/meal/:mealID", controllerMeals.getMealInformation);

router.delete("/studenthome/:id/meal/:mealID", auth.validateToken, controllerMeals.deleteMeal);

router.post("/studenthome/:homeId/meal/:mealId/signup", auth.validateToken, controllerSignUp.signupForMeal)

router.delete("/studenthome/:homeId/meal/:mealId/signoff", auth.validateToken, controllerSignUp.signoffForMeal);

router.get("/meal/:mealId/participants", auth.validateToken, controllerSignUp.getParticipants);

router.get("/meal/:mealId/participants/:participantId", auth.validateToken, controllerSignUp.getDetailsParticipants);


module.exports = router;
