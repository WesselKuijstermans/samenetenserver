const chai = require("chai");
const mocha = require("mocha");
const timeToWait = 1500;
const chaiHttp = require("chai-http");
const server = require("../server");
const database = require("../dao/database");
const assert = require("assert");
const logger = require('tracer').console()
const insert = require('./fillTestDatabaseQueries')
const jwt = require('jsonwebtoken')
const { readdirSync } = require("fs");

chai.should();
chai.use(chaiHttp);

mocha.before((done) => {
	database.query(insert.CLEAR_DB, (err, rows, fields) => {
		if (err) {
			logger.log("CLEARING")
			logger.error(`before CLEARING tables: ${err}`)
			done(err)
		} else {
			logger.log("CLEARING")
			done()
		}
	})
})
mocha.after((done) => {
	database.query(insert.CLEAR_DB, (err, rows, fields) => {
		if (err) {
			logger.log("CLEARING 2")
			console.log(`after error: ${err}`)
			done(err)
		} else {
			logger.log("CLEARING 2")
			logger.info("After FINISHED")
			done()
		}
	})
})

describe("Authenticatie", function () {
	mocha.before((done) => {
		database.query(insert.INSERT_USER, (err, rows, fields) => {
			if (err) {
				logger.error(`before INSERT_USER: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before INSERT_USER done`)
				done()
			}
		})
	})
    describe("post", function () {
        it("TC-101-1 Verplicht veld ontbreekt", (done) => {
            chai.request(server)
                .post("/api/register/")
                .query({
                    firstname: "luuk",
                    lastname: "bartels1",
                    studentnr: "21697077",
                    password: "cooleman34"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-101-2 Invalide email adres", (done) => {
            chai.request(server)
                .post("/api/register/")
                .query({
                    firstname: "Tim",
                    lastname: "De Laater",
                    email: "cooleman34cool.com",
                    studentnr: "21693512",
                    password: "timmieman12"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-101-3 Invalide wachtwoord", (done) => {
            chai.request(server)
                .post("/api/register/")
                .query({
                    firstname: "Tim",
                    lastname: "De Laater",
                    email: "cooleman34@cool.com",
                    studentnr: "21693512",
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-101-4 Gebruiker bestaat al", (done) => {
            chai.request(server)
                .post("/api/register/")
                .query({
                    firstname: "Marieke",
                    lastname: "van Dam",
                    email: "mariekevandam@home.nl",
                    studentnr: "12345",
                    password: "secretsecret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-101-5 Gebruiker succesvol geregistreerd", (done) => {
            chai.request(server)
                .post("/api/register/")
                .query({
                    firstname: "Wouter",
                    lastname: "Zegers",
                    email: "Wouter@server3.nl",
                    studentnr: "12356",
                    password: "secretsecret"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-102-1 Verplicht veld ontbreekt", (done) => {
            chai.request(server)
                .post("/api/login/")
                .query({
                    email: "jsmit@server.nl",
                })
                .end((err, res) => {
                    res.should.have.status(422)
                    done()
                })
        })
        it("TC-102-2 Invalide email adres", (done) => {
            chai.request(server)
                .post("/api/login/")
                .query({
                    email: "jsmit@servernl",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-102-3 Invalide wachtwoord", (done) => {
            chai.request(server)
                .post("/api/login/")
                .query({
                    email: "jsmit@server.nl",
                    password: "secert"
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-102-4 Gebruiker bestaat niet", (done) => {
            chai.request(server)
                .post("/api/login/")
                .query({
                    email: "wesselKuijstermans@hotmail.com",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-102-5 Gebruiker succesvol ingelogd", (done) => {
            chai.request(server)
                .post("/api/login/")
                .query({
                    email: "dion@jansen.nl",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })

})