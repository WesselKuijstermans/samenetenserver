const chai = require("chai")
const mocha = require("mocha")
const timeToWait = 1500
const chaiHttp = require("chai-http")
const server = require("../server")
const database = require("../dao/database")
const assert = require("assert")
const logger = require('tracer').console()
const insert = require('./fillTestDatabaseQueries')
const jwt = require('jsonwebtoken')
const { readdirSync } = require("fs")

chai.should();
chai.use(chaiHttp);

describe("meals", function () {
    mocha.before((done) => {
        database.query(insert.INSERT_STUDENTHOME,(err,rows,fields) => {
            if(err){
                logger.error(`Before, insert studenthome query: ${err}`)
                done(err)
            }
            if(rows){
                done()
            }
        })
    })
    mocha.before((done) => {
        database.query(insert.INSERT_MEAL,(err,rows,fields) => {
            if(err){
                logger.error(`Before, insert meal query: ${err}`)
                done(err)
            }
            if(rows){
                done()
            }
        })
    })
    describe("post", function () {
        it("TC-301-1 Verplicht veld ontbreekt", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "AVG",
                    ingredienten: "aardappel,vlees,groente",
                    allergien: "boter,kaas,eieren",
                    prijs: "24,99",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-301-2 Niet ingelogd", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal")
                .query({
                    naam: "AVG",
                    beschrijving: "lekker aardappel vlees en groente",
                    ingredienten: "aardappel,vlees,groente",
                    allergien: "boter,kaas,eieren",
                    prijs: "24,99",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-301-3 Maaltijdsuccesvol toegevoegd", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "AVG",
                    beschrijving: "lekker aardappel vlees en groente",
                    ingredienten: "aardappel,vlees,groente",
                    allergien: "boter,kaas,eieren",
                    prijs: "24,99",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("put", function () {
        it("TC-302-1 Verplicht veld ontbreekt", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/3")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "AVG",
                    beschrijving: "lekker aardappel vlees en groente",
                    ingredienten: "aardappel,vlees,groente",
                    allergien: "boter,kaas,eieren",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-302-2 Niet ingelogd", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/3")
                .query({
                    naam: "AVG",
                    beschrijving: "lekker aardappel vlees en groente",
                    ingredienten: "aardappel,vlees,groente",
                    allergien: "boter,kaas,eieren",
                    prijs: "24,99",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-302-3 Niet de eigenaar van de data", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/3")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
                .query({
                    naam: "AVG",
                    beschrijving: "lekker aardappel vlees en groente",
                    ingredienten: "aardappel,vlees,groente",
                    allergien: "boter,kaas,eieren",
                    prijs: "24,99",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-302-4 Maaltijd bestaat niet", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/303")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "AVG",
                    beschrijving: "lekker aardappel vlees en groente",
                    ingredienten: "aardappel,vlees,groente",
                    allergien: "boter,kaas,eieren",
                    prijs: "24,99",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-302-5 Maaltijd succesvol gewijzigd", (done) => {
            chai.request(server)
                .put("/api/studenthome/1/meal/3")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .query({
                    naam: "Sushi",
                    beschrijving: "Vis",
                    ingredienten: "zalm",
                    allergien: "boter,kaas,eieren",
                    prijs: "24,99",
                    aantalPersonen: "8",
                    aangebodenOp: "2021-05-19"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("Get", function () {
        it("TC-303-1 Lijst van maaltijden geretourneerd", (done) => {
            chai.request(server)
                .get("/api/studenthome/1/meal")
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-304-1 Maaltijd bestaat niet", (done) => {
            chai.request(server)
                .get("/api/studenthome/1/meal/303")
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-304-2 Details van maaltijdgeretourneerd", (done) => {
            chai.request(server)
                .get("/api/studenthome/1/meal/3")
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    describe("Delete", function () {
        it("TC-305-2 Niet ingelogd", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/3")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-305-3 Niet de eigenaar van de data", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/3")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
        it("TC-305-4 Maaltijd bestaat niet", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/393")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
        it("TC-305-5 Maaltijd succesvol verwijderd", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/3")
                .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })

})



