const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../server")
const jwt = require("jsonwebtoken")
const assert = require("assert")
const pool = require("../dao/database")
const logger = require('tracer').console()
const insert = require("./fillTestDatabaseQueries")
chai.should()
chai.use(chaiHttp)


// UC-401 Aanmelden voor maaltijd
// TC-401-1 Niet ingelogd
// Geen aanmelding toegevoegdResponsestatus HTTP code 401
// Response bevat JSON object met daarin generieke foutinformatie.

describe("Participants", function () {
    before((done) => {
		pool.query(insert.INSERT_PARTICIPANTS, (err, rows, fields) => {
			if (err) {
				logger.error(`before INSERT_PARTICIPANTS: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before INSERT_PARTICIPANTS done`)
				done()
			}
		})
	})
    describe("Sign up", function () {
        it("TC-401-1 should return a 401 when not signed in", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal/1/signup")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
    })


    // TC-401-2 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    describe("Sign up", function () {
        it("TC-401-2 should return a 404 when meal not found.", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal/666/signup")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
    })

    // TC-401-3 Succesvol aangemeld
    // Aanmelding is toegevoegdResponsestatus HTTP code 200 (OK)
    // Response bevat JSON object metalle gegevens van de aanmelding.

    describe("Sign up", function () {
        it("TC-401-3 should return a 200 when succesfull.", (done) => {
            chai.request(server)
                .post("/api/studenthome/1/meal/2/signup")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
    // UC-402 Afmelden voor maaltijd
    // TC-402-1 Niet ingelogd
    // Geen aanmelding toegevoegdResponsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    describe("Sign off", function () {
        it("TC-402-1 should return a 401 when not signed in", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/1/signoff")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
    })

    // TC-402-2 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    describe("Sign off", function () {
        it("TC-402-2 should return a 404 when meal not found.", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/666/signoff")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
    })

    // TC-402-3 Aanmelding bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    describe("Sign off", function () {
        it("TC-402-2 should return a 404 when registration not found.", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/666/signoff")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {
                    res.should.have.status(404)
                    done()
                })
        })
    })

    // TC-403-4 Succesvol afgemeld
    // Aanmelding is verwijderd
    // Responsestatus HTTP code 200 (OK)

    describe("Sign off", function () {
        it("TC-402-4 should return a 200 when sign off succesfull.", (done) => {
            chai.request(server)
                .delete("/api/studenthome/1/meal/2/signoff")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })

    // UC-403 Lijst van deelnemers opvragen
    // TC-403-1 Niet ingelogd
    // Responsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    describe("Get participants", function () {
        it("TC-403-1 should return a 401 when not signed in", (done) => {
            chai.request(server)
                .get("/api/meal/1/participants")
                .end((err, res) => {
                    res.should.have.status(401)
                    done()
                })
        })
    })

    // TC-403-2 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie

    describe("Get participants", function () {
        it("TC-403-2 should return a 404 when not found", (done) => {
            chai.request(server)
                .get("/api/meal/3/participants")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.contain({
                        Message: 'No participants found',
                    });
                    done()
                })
        })
    })

    // TC-403-3 Lijst van deelnemers geretourneerd
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object met contactgegevens van alle aangemelde gebruikers.
    // Response bevat niet het wachtwoord van de deelnemers.

    describe("Get participants", function () {
        it("TC-403-3 should return a 200 when participants found", (done) => {
            chai.request(server)
                .get("/api/meal/2/participants")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {

                    res.should.have.status(200)
                    // .that.includes.all.keys(['UserID', 'StudenthomeID', 'Student_Number','First_Name' , 'Last_Name','SignedUpOn'])
                    done()
                })
        })
    })

    describe("Get participant by id", function () {
        it("TC-404-1 should return a 401 when not logged in", (done) => {
            chai.request(server)
                .get("/api/meal/2/participants/2")
                .end((err, res) => {

                    res.should.have.status(401)
                    // .that.includes.all.keys(['UserID', 'StudenthomeID', 'Student_Number','First_Name' , 'Last_Name','SignedUpOn'])
                    done()
                })
        })
    })

    describe("Get participant by id", function () {
        it("TC-404-2 should return a 404 when participant not found", (done) => {
            chai.request(server)
                .get("/api/meal/2/participants/10")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {

                    res.should.have.status(404)
                    // .that.includes.all.keys(['UserID', 'StudenthomeID', 'Student_Number','First_Name' , 'Last_Name','SignedUpOn'])
                    done()
                })
        })
    })

    describe("Get participant by id", function () {
        it("TC-404-3 should return a 200 when participants found", (done) => {
            chai.request(server)
                .get("/api/meal/2/participants/2")
                .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
                .end((err, res) => {

                    res.should.have.status(200)
                    // .that.includes.all.keys(['UserID', 'StudenthomeID', 'Student_Number','First_Name' , 'Last_Name','SignedUpOn'])
                    done()
                })
        })
    })
})
