const mysql = require('mysql')
const logger = require('tracer').console();
const dbconfig = require('./databaseConfig').dbconfig
const pool = mysql.createPool(dbconfig)

pool.on('connection', function (connection) {
  logger.log('Database connection established')
})

pool.on('acquire', function (connection) {
  logger.log('Database connection aquired')
})

pool.on('release', function (connection) {
  logger.log('Database connection released')
})

module.exports = pool